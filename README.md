XBOX EEPROM reader / writer
======================

A small Arduino sketch used to read or write an original XBOX EEPROM (or literally any I2C based EEPROM).


Wiring
-----------

I only tried to read / write unsoldered EEPROMS, always check voltages before frying your box accidentally ;)

Also check the datasheet for your specific EEPROM as pinouts may differ.

Common Arduino wiring goes like this:

* `VCC (+5V)` to `VCC` on your EEPROM
* `GND` to `GND` on your EEPROM
* `SDA` from your EEPROM to analog pin `A4` (may differ based on your Arduino board)
* `SCL` from your EEPROM to analog pin `A5` (may differ based on your Arduino board)


Sketch configuration
-------------------

* `EEPROM_ADDR` the I2C adress of your EEPROM (you may need to use an [I2C scanner](http://playground.arduino.cc/Main/I2cScanner) to find your adress but unsoldered XBOX EEPROMS are always `0x50`)
* `EEPROM_BACKUP` 256 bytes hexadecimal array containing your backup (only needed if you want to flash)

Usage
-------------------
* Wire up your EEPROM, flash your Arduino with this sketch and fire up the Serial Monitor with 9600 Baud.
* Wait for the main menu to appear and choose your option
    1. `Dump / read current contents of EEPROM` Creates your typical hexadecimal table with the contents of your EEPROM
    2. `Flash EEPROM contained in EEPROM_BACKUP` Flashes everything contained in `EEPROM_BACKUP` onto your EEPROM
    3. `Erase EEPROM` Overwrites all 256 bytes with `0xFF`
    4. `Restart`Throws you back into the menu