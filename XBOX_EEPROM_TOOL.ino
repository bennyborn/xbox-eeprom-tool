/* 
  * EEPROM 4 (GND) to GND
  * EEPROM 8 (Vcc) to Vcc (5 Volts)
  * EEPROM 5 (SDA) to Arduino Analog Pin 4
  * EEPROM 6 (SCL) to Arduino Analog Pin 5
  * EEPROM 7 (WP)  to GND
  * EEPROM 1 (A0)  to GND
  * EEPROM 2 (A1)  to GND
  * EEPROM 3 (A2)  to GND
  */

#include <Wire.h>

const byte EEPROM_ADDR = 0x50;

byte EEPROM_BACKUP[256] = {
0x22,0x63,0x3F,0x75,0x46,0x2C,0x3C,0x14,0xDB,0x2E,0x07,0x28,0x34,0xA6,0x07,0xE5,
0x42,0xF2,0xFC,0xDD,0x1A,0x6B,0x7A,0xA8,0x2C,0x11,0x49,0xF8,0x78,0x41,0x71,0xEB,
0xD9,0x25,0x88,0x06,0xC7,0xF4,0x79,0x6A,0xC9,0x5F,0x7E,0x4A,0xC2,0xA4,0x41,0x90,
0x15,0x71,0xBF,0xCF,0x33,0x31,0x32,0x36,0x33,0x33,0x33,0x32,0x33,0x38,0x30,0x35,
0x00,0x12,0x5A,0x1E,0xBD,0x9E,0x00,0x00,0x39,0x4B,0x83,0xD9,0xCB,0x0C,0x45,0x4F,
0x06,0x9C,0xAB,0x32,0x88,0x4A,0x5C,0x18,0x00,0x03,0x80,0x00,0x00,0x00,0x00,0x00,
0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const char MENU[] = "__  __  ___    ___   __  __\n"
"\\ \\/ / | _ )  / _ \\  \\ \\/ /\n"
" >  <  | _ \\ | (_) |  >  < \n"
"/_/\\_\\ |___/  \\___/  /_/\\_\\\n"
"\n"
"  EEPROM READER / WRITER\n"
"\n"
"1) Dump / read current contents of EEPROM\n"
"2) Flash EEPROM contained in EEPROM_BACKUP\n"
"3) Erase EEPROM\n"
"4) Restart\n"
"\n"
"Your choice [1-4]: \n";

void action_menu()
{
    Serial.print(MENU);
}

void action_dump() {
    Serial.print("\n\n");
    eeprom_dump(EEPROM_ADDR, 0,256);
    Serial.print("\n\n");
    delay(2000);
}

void action_flash() {

    char choice = ' ';

    Serial.println("\n\n!!! WARNING !!!");
    Serial.println("This will overwrite all the contents of your EEPROM!");
    Serial.println("Do you want to continue (y/n)?\n\n");

    while( !Serial.available() ) {}

    choice = Serial.read();

    if( choice == 'Y' || choice == 'y' ) {

        Serial.print("Flashing backup onto EEPROM...");

        for( int i=0; i<sizeof(EEPROM_BACKUP); i++ ) {
            eeprom_write_byte(EEPROM_ADDR, i, EEPROM_BACKUP[i]);
        }

        Serial.print("DONE!\n\n");
        delay(2000);
    }
}

void action_erase() {

    char choice = ' ';

    Serial.println("\n\n!!! WARNING !!!");
    Serial.println("This will erase all the contents of your EEPROM!");
    Serial.println("Do you want to continue (y/n)?\n\n");

    while( !Serial.available() ) {}

    choice = Serial.read();

    if( choice == 'Y' || choice == 'y' ) {

        Serial.print("Erasing contents of your EEPROM...");

        for( int i=0; i<256; i++ ) {
            eeprom_write_byte(EEPROM_ADDR, i, 0xFF);
        }

        Serial.print("DONE!\n\n");
        delay(2000);
    }
}

void setup()
{    
    Wire.begin();
    Serial.begin(9600);

    action_menu();
}

void loop()
{
    
    char choice = ' ';

    if( Serial.available() > 0 ) {

        choice = Serial.read();

        if( choice == '1' ) {
            action_dump();
        } else if( choice == '2' ) {
            action_flash();
        } else if( choice == '3' ) {
            action_erase();
        }

        action_menu();
    }
}


void eeprom_write_byte(byte deviceaddress, int eeaddress, byte data)
{
    byte devaddr = deviceaddress | ((eeaddress >> 8) & 0x07);
    byte addr    = eeaddress;
    Wire.beginTransmission(devaddr);
    Wire.write(int(addr));
    Wire.write(int(data));
    Wire.endTransmission();
    delay(10);
}

int eeprom_read_buffer(byte deviceaddr, unsigned eeaddr, byte * buffer, byte length)
{
    // Three lsb of Device address byte are bits 8-10 of eeaddress
    byte devaddr = deviceaddr | ((eeaddr >> 8) & 0x07);
    byte addr    = eeaddr;
    
    Wire.beginTransmission(devaddr);
    Wire.write(int(addr));
    Wire.endTransmission();

    Wire.requestFrom(devaddr, length);
    int i;
    for (i = 0; i < length && Wire.available(); i++) {
        buffer[i] = Wire.read();
    }
    return i;
}

void eeprom_dump(byte devaddr, unsigned addr, unsigned length)
{
    unsigned startaddr = addr & (~0x0f);
    unsigned stopaddr  = (addr + length + 0x0f) & (~0x0f);

    for( unsigned i = startaddr; i < stopaddr; i += 16 ) {
        byte buffer[16];
        char outbuf[6];
        sprintf(outbuf, "%03X: ", i);
        Serial.print(outbuf);
        eeprom_read_buffer(devaddr, i, buffer, 16);
        for (int j = 0; j < 16; j++) {
            if (j == 8) {
                Serial.print(" ");
            }
            sprintf(outbuf, "%02X ", buffer[j]);
            Serial.print(outbuf);            
        }
        Serial.print(" |");
        for (int j = 0; j < 16; j++) {
            if (isprint(buffer[j])) {
              char ascii_char = buffer[j];
                Serial.print(ascii_char);
            }
            else {
                Serial.print('.');
            }
        }
        Serial.println("|");
    }
}
